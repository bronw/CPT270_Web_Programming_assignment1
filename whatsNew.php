<!DOCTYPE html>
<html lang="en">
<head> <!-- repeated throughout  -->
    <meta charset="UTF-8">
    <title>whats new</title>

    <link rel="stylesheet" type="text/css"
          href="resources/styles/generalStyle.css" />

    <!-- fonts -->
    <link href='https://fonts.googleapis.com/css?family=Simonetta:400,900italic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css"
          href="http://fonts.googleapis.com/css?family=Tangerine">






</head>
<body>
<!-- Defining the #page section-->
<section class="page">
    <!-- header fragment  -->
    <article class = "headingFragment">
        <?php require('resources/fragments/header.php') ?>
    </article>


    <article class = "whatsNew">

        <H1>Time to Cut the Rug </H1>
        <figure id = "whatsNew1">
            <img
                src="resources\img\Don_whisperItToMe.jpg"
                width = 300px
                alt="did you say that">
        </figure>


        <p>The place is real flash now. <br>
            Our re-opening is a party and nothing but the best for
            our guests. Who knows... maybe Johnny Fontane might show up and sing
            a few numbers. </p>


    <h2>Great Chairs</h2>
        <p>We have installed new seats.</p>


        <figure  id = "whatsNew4">
            <img
                src="resources/img/Fredo1.jpg"
                width = 100px
                alt="TheFro">
        </figure>

        <p>We have exclusive gold class seats - just like Michael's favourite.
            </p>

        <p> And there is nothing standard about our standard. They are good
            enough for your best friend (or your worst enemy).
        </p>
        <p> And we have bean-bags! <br>
            Which you can share - Fredo specially likes those.
        </p>

    <h2 class ="clearItLeft">Eye popping Projectors</h2>
        <p>It doesn't stop there! we have also installed a sophisticated
            3D <del>surveillance</del> projection system.
        </p>
        <p>The images of Sonny stamping his ticket at the Candy Island toll booth
            with stay with you for the rest of your life.
        </p>

        <figure id = "whatsNew2">
            <img
                src="resources/img/candybar_Godfather_Luca_Brasi_by_donvito62.jpg"
                width = 300px
                alt="sleepsWithFish">
        </figure>

    <h2>Bigger Sound</h2>
        <p>We have you surrounded - with sound!
        </p>

        <p>Dolby Cinema Surround Sound! Wow! Cutting heads off horses
            has never sounded so good,
            <a href="http://www.dolby.com/us/en/cinema/index.html">
                check it out! </a>.
        </p>

        <figure  id = "whatsNew3">
            <img
                src="resources/img/deadHorse_article-godfather2-0405.JPG"
                width = 300px
                alt="horseRacing">
        </figure>


        <p> Because, as Don Vito Corleone says, a man that doesn't spend time with his
            family - at the movies,

            <span = class = "theFamily">  he can never be a real man. </span>
        </p>




        <br>
        <a href ="nowShowing.php" class ="bigLink">See a show!</a>










    </article>



    <!-- footer fragment  -->
    <article class = "footerFragment">
        <?php require('resources/fragments/footer.php') ?>
    </article>



</section>
</body>
</html>
/**
 * Created by Anthony on 10/01/2016.
 */

/*Price constants  */
PGROUP1_goldAdult = 25;
PGROUP1_goldChild = 20;
PGROUP1_StdFull = 12;
PGROUP1_StdConssion = 10;
PGROUP1_StdChild = 8;
PGROUP1_beanie1 = 20;
PGROUP1_beanie2 = 20;
PGROUP1_beanie3 = 20;

PGROUP2_goldAdult = 30;
PGROUP2_goldChild = 25;
PGROUP2_StdFull = 18;
PGROUP2_StdConssion = 15;
PGROUP2_StdChild = 12;
PGROUP2_beanie1 = 25;
PGROUP2_beanie2 = 25;
PGROUP2_beanie3 = 25;


/** this function returns information from the movie select table
 * and  puts it into the form fields**/
function updateMovie(day,time,genre,priceGroup){

    recordDayTimeName(day,time,genre,priceGroup);
    var mTitle = showMovieAndRecordTitle(genre);
    var movieText= makeMovieString(mTitle);
    recordMovieInfo(movieText);

    // ticket information
    updateTicketPrices(priceGroup);
    var ticketText = makeTicketText(priceGroup);

    presentStr(movieText + ticketText);

}


/* functions for recording the tickets selected   */
function recordTicket(ticketCode){

    addTicket(ticketCode);
    var ticketText = makeTicketTextWithoutPriceGroup();
    var movieText= getRecordedMovieString();
    presentStr(movieText + ticketText);


}

function recordDayTimeName(day,time,genre,priceGroup) {
    document.getElementById('day').value = day;
    document.getElementById('time').value = time;
    document.getElementById('name').value = genre;
    document.getElementById('priceGroup').value = priceGroup;
    document.getElementById('movie').value = genre;
}

function showMovieAndRecordTitle(genre){
/** the movie picture and title are recorded in
*   the movie display fragment
**/

    // hide all the cards
    var allCards = document.getElementsByClassName("MovieCard");
    var i;
    for (i = 0; i < allCards.length; i++) {
            allCards[i].style.display = "none";
    }

    // show the right card
    var classStr2="MovieCard " + genre;
    var allCards2 = document.getElementsByClassName(classStr2);
    allCards2[0].style.display = "block";

    // and find the movie title in the correct card
    var classStr="desc " + genre;
    var x = document.getElementsByClassName(classStr);

    //record it and return it
    document.getElementById('movieTitle').value = x[0].innerHTML;
    return x[0].innerHTML;

}


function recordMovieInfo(buildStr) {
    document.getElementById('movieString').value = buildStr;
}

function  getRecordedMovieString(){

    var text ="";
    var movieString = document.getElementById('movieString').value;
    if (movieString.length <2){
        enableSubmit(false);
        text = "You have not selected a movie. ";
    }else{
        enableSubmit(true);
        text=movieString ;
    }
    return text + "\r";
}

function  makeMovieString(mTitle){

    var text ="";
    var day = document.getElementById('day').value;
    var time = document.getElementById('time').value;
    text = "Off to see " + mTitle +
        " at " + time +
        " on " + day + ". \r";


    return text;
}



function presentStr(builtStr){
    document.getElementById('displayOrder').textContent= builtStr;
}





/** bit code for adding the selected ticket to records   */
 function addTicket(ticketCode) {

/*    var x= parseInt(document.getElementById(ticketCode).value);
    document.getElementById(ticketCode).value = (isNaN(x)) ? 1:x+1;*/

    var x= parseInt(document.getElementsByName(ticketCode)[0].value);
    document.getElementsByName(ticketCode)[0].value = (isNaN(x)) ? 1:x+1;

}


function makeTicketTextWithoutPriceGroup() {

    var priceGroup = document.getElementById('priceGroup').value ;
    return makeTicketText(priceGroup);
}
/** this function counts the ticket requests and makes a summary **/
function makeTicketText(priceGroup){

   var priceList = getpriceList(priceGroup);
    var text ="Your Tickets:\r";
    var ticketCount;
    var ticketCost;
    var ticketType;
    var ticketDescr;
    var haveTickets;
    var ticketTotal = 0;

    // iterate the document elements
    var allTickets= document.getElementsByClassName("ticketInfo");

    var i;
    for (i = 0; i < allTickets.length; i++) {

        //text = text +priceList[allTickets[i].id]  +"\r";

        ticketType =allTickets[i].id; //change this to name
        ticketCount= parseInt(allTickets[i].value);
        //ticketCount= parseInt(document.getElementById(ticketType).value);
        ticketCount = (isNaN(ticketCount)) ? 0:ticketCount;

        if (ticketCount){
            haveTickets = true;
            ticketDescr =allTickets[i].id; //change this to ID
            ticketCost = parseInt(priceList[allTickets[i].name]) //change this to name;
            ticketTotal += ticketCount*ticketCost;

            text = text + ticketCount + " " + ticketDescr + " at $" + ticketCost +
                        ".00 ($" + (ticketCount*ticketCost) + ".00)\r" ;

        }
    }

   if (!(haveTickets)){
       enableSubmit(false);
       text = "You have not picked any tickets yet.\r"
   }else{
       enableSubmit(true);
       text = text + "for a total of $" + ticketTotal +".00\r";
   }

    return text;
    // testing
    // document.getElementById("1").innerHTML = text;


}



/** use Price constants to make object  **/
function getpriceList(priceGroup){

    if (priceGroup=="PGROUP1") {
        var priceList = {
            SA : PGROUP1_StdFull,
            SP : PGROUP1_StdConssion,
            SC : PGROUP1_StdChild,
            FA : PGROUP1_goldAdult,
            FC : PGROUP1_goldChild,
            B1 : PGROUP1_beanie1,
            B2 : PGROUP1_beanie2,
            B3 : PGROUP1_beanie3
        }
    } else if (priceGroup=="PGROUP2") {
        var priceList = {
            SA : PGROUP2_StdFull,
            SP : PGROUP2_StdConssion,
            SC : PGROUP2_StdChild,
            FA : PGROUP2_goldAdult,
            FC : PGROUP2_goldChild,
            B1 : PGROUP2_beanie1,
            B2 : PGROUP2_beanie2,
            B3 : PGROUP2_beanie3
            }
    } else {
        var priceList = {
            SA : 0,
            SP : 0,
            SC : 0,
            FA : 0,
            FC : 0,
            B1 : 0,
            B2 : 0,
            B3 : 0
         }

    }
    return priceList;
}



function enableSubmit(trueForEnable){

    document.getElementById("submitBtn").disabled = !(trueForEnable);


}


function submitSelectRecordsFrom(){

    document.getElementById("selectionRecords").submit();
}

function resetSelectRecordsFrom(){

    document.getElementById("selectionRecords").reset();
    clearOrderDetails();
    enableSubmit(false);
}
function clearOrderDetails(){
    document.getElementById("displayOrder").innerHTML =null;
}














/* function for updating the displayed ticket prices  */
function updateTicketPrices(priceGroup){


if (priceGroup == "PGROUP1"){

    document.getElementById('goldAdult').innerHTML = PGROUP1_goldAdult;
    document.getElementById('goldChild').innerHTML = PGROUP1_goldChild ;
    document.getElementById('StdFull').innerHTML = PGROUP1_StdFull ;
    document.getElementById('StdConssion').innerHTML = PGROUP1_StdConssion ;
    document.getElementById('StdChild').innerHTML = PGROUP1_StdChild;
    document.getElementById('beanie1').innerHTML = PGROUP1_beanie1;
    document.getElementById('beanie2').innerHTML = PGROUP1_beanie2;
    document.getElementById('beanie3').innerHTML = PGROUP1_beanie3;
} else{
    document.getElementById('goldAdult').innerHTML = PGROUP2_goldAdult;
    document.getElementById('goldChild').innerHTML = PGROUP2_goldChild ;
    document.getElementById('StdFull').innerHTML = PGROUP2_StdFull ;
    document.getElementById('StdConssion').innerHTML = PGROUP2_StdConssion ;
    document.getElementById('StdChild').innerHTML = PGROUP2_StdChild;
    document.getElementById('beanie1').innerHTML = PGROUP2_beanie1;
    document.getElementById('beanie2').innerHTML = PGROUP2_beanie2;
    document.getElementById('beanie3').innerHTML = PGROUP2_beanie3;
}
}

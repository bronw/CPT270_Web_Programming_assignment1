<!DOCTYPE html>
<html lang="en">
<head>
    <link rel = "stylesheet"
          href="resources/styles/MovieSelectTableStyle.css" >
</head>
<body>
        <!--<button type="button" onclick= updateMovie("Monday","1PM","RC","PGROUP1")> testInside</button>-->
        <!--<button type="button" onclick= updateMovie("Saturday","6PM","AC","PGROUP2")> testInsideGroup2</button>-->
<table id ="pickMovieTable">
    <tr>
        <th class="blankCell"></th>
        <th colspan="2">midday</th>
        <th >afternoon</th>
        <th >evening</th>
        <th>night</th>
    </tr>

    <tr>
        <th class="blankCell"></th>
        <th >12:00 PM</th>
        <th >1:00 PM</th>
        <th >3:00 PM</th>
        <th >6:00 PM</th>
        <th>9:00 PM</th>
    </tr>

    <tr>
        <th>Monday</th>
        <td class="blankCell"></td>
        <td id = "kid"
            onclick= updateMovie("Monday","1PM","CH","PGROUP1")>
            kid</td>
        <td class="blankCell"></td>
        <td id = "for"
            onclick= updateMovie("Monday","6PM","AF","PGROUP1")>
            for </td>
        <td id = "romcom"
            onclick= updateMovie("Monday","9PM","RC","PGROUP1")>
            romcom</td>
    </tr>

    <tr>
        <th>Tuesday</th>
        <td class="blankCell"></td>
        <td id = "kid"
            onclick= updateMovie("Tuesday","1PM","CH","PGROUP1")>
            kid</td>
        <td class="blankCell"></td>
        <td id = "for"
            onclick= updateMovie("Tuesday","6PM","AF","PGROUP1")>
            for </td>
        <td id = "romcom"
            onclick= updateMovie("Tuesday","9PM","RC","PGROUP1")>
            romcom</td>

    </tr>

    <tr>
        <th>Wednesday</th>
        <td class="blankCell"></td>
        <td  id = "romcom"
            onclick= updateMovie("Wednesday","1PM","RC","PGROUP1")>
            romcom</td>
        <td class="blankCell"></td>
        <td  id = "kid"
            onclick= updateMovie("Wednesday","6PM","CH","PGROUP2")>
            kid</td>
        <td  id = "act"
            onclick= updateMovie("Wednesday","9PM","AC","PGROUP2")>
            act</td>
    </tr>

    <tr>
        <th>Thursday</th>
        <td class="blankCell"></td>
        <td  id = "romcom"
            onclick= updateMovie("Thursday","1PM","RC","PGROUP1")>
            romcom</td>
        <td class="blankCell"></td>
        <td  id = "kid"
            onclick= updateMovie("Thursday","6PM","CH","PGROUP2")>
            kid</td>
        <td  id = "act"
            onclick= updateMovie("Thursday","9PM","AC","PGROUP2")>
            act</td>
    </tr>

    <tr>
        <th>Friday</th>
            <td class="blankCell"></td>
            <td  id = "romcom"
                onclick= updateMovie("Friday","1PM","RC","PGROUP1")>
                romcom</td>
            <td class="blankCell"></td>
            <td  id = "kid"
                onclick= updateMovie("Friday","6PM","CH","PGROUP2")>
                kid</td>
            <td  id = "act"
                onclick= updateMovie("Friday","9PM","AC","PGROUP2")>
                act</td>
    </tr>

    <tr>
        <th>Saturday</th>
        <td id = "kid"
            onclick= updateMovie("Saturday","12PM","CH","PGROUP2")>
            kid</td>
        <td class="blankCell"></td>
        <td  id = "for"
             onclick= updateMovie("Saturday","3PM","AF","PGROUP2")>
            for </td>
        <td  id = "romcom "
             onclick= updateMovie("Saturday","6PM","RC","PGROUP2")>
            romcom</td>
        <td id = "act"
            onclick= updateMovie("Saturday","9PM","AC","PGROUP2")>
            act</td>
    </tr>

    <tr>
        <th>Sunday</th>
        <td id = "kid"
            onclick= updateMovie("Sunday","12PM","CH","PGROUP2")>
            kid</td>
        <td class="blankCell"></td>
        <td  id = "for"
             onclick= updateMovie("Sunday","3PM","AF","PGROUP2")>
            for </td>
        <td  id = "romcom"
             onclick= updateMovie("Sunday","6PM","RC","PGROUP2")>
            romcom</td>
        <td id = "act"
            onclick= updateMovie("Sunday","9PM","AC","PGROUP2")>
            act</td>
    </tr>



</table>
</body>



</html>
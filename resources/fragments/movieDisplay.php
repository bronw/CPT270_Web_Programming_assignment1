<!DOCTYPE html>
<html lang="en">
<head>
    <link rel = "stylesheet"
          href="resources/styles/MovieDisplayStyle.css" >
</head>
<body>
<article id="MovieCards">
    <div class="MovieCard RC">
        <div><H3 class="desc RC">Love Actually</H3></div>
        <img class="pic" src="resources/img/loveActually2.jpg" alt="Romantics" width="300" height="200">
    </div>

    <div class="MovieCard CH">
        <div><H3 class="desc CH">Brave</H3></div>
        <img class="pic" src="resources/img/brave-620x266.png" alt="child" width="300" height="200">
    </div>

    <div class="MovieCard AC">
        <div><H3 class="desc AC">Goldfinger</H3></div>
        <img class="pic" src="resources/img/goldfinger_poster.jpg" alt=action"  width="300" height="200">
    </div>

    <div class="MovieCard AF">
        <div><H3 class="desc AF">Kamasutra 3D</H3></div>
        <img class="pic" src="resources/img/Kamasutra-3D-poster.jpg" alt="Artie" width="300" height="200" >
    </div>
</article>
</body>

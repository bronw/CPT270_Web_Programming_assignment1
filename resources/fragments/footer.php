<!DOCTYPE html>
<!--universal header for the Silverado page -->
<head>
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300italic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css"
          href="resources/styles/footerStyle.css" />
</head>

<footer>


    <!-- Dividing line-->
    <div class="line" id="footerLine"></div>

    <!-- Rose-->
    <img id = "rosePic2"
         src="resources/img/roseVersion5.jpg"

         alt="rose"
         style="width:48px;height:48px;">

    </div>


    <!--  footer words-->
    <div id="footerNav">
            <div id="contactBtn" >
                <a href=contacts.php> Contact us </a>
            </div>
            <div id="byLine" >
                by:  Anthony Brown s3460996
            </div>

    </div>

</footer>




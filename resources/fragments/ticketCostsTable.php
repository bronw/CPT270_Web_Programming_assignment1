<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel = "stylesheet"
          href="resources/styles/ticketSelectTableStyle.css">
</head>

<!--<button type="button" onclick= recordTicket("SA")> TEST Add SA</button>-->
<!--<button type="button" onclick= recordTicket("FA")> TEST Add SA</button>-->


<table id ="pickTicketTable">
    <tr>
        <th>Seating</th>
        <th colspan="2">Gold Class</th>
        <th colspan="3">Standard</th>
        <th colspan="3">Bean-Bag*</th>
    </tr>

    <tr>
        <th>Ticket</th>
        <td>
            <article class="sub" onclick= recordTicket("FA")>
                    <p>adult</p>
                    <p class = "ticketValue" id="goldAdult">0</p>
            </article>
        </td>
        <td>
            <article class="sub" onclick= recordTicket("FC")>
                <p>child</p>
                <p class = "ticketValue" id="goldChild">0</p>
            </article>
        </td>
        <td>
            <article class="sub" onclick= recordTicket("SA")>
                <p>full</p>
                <p class = "ticketValue" id="StdFull">0</p>
            </article>
        <td>
            <article class="sub" onclick= recordTicket("SP")>
                <p>concession</p>
                <p class = "ticketValue" id="StdConssion">0</p>
            </article>
        </td>
        <td>
            <article class="sub" onclick= recordTicket("SC")>
                <p>child</p>
                <p class = "ticketValue" id="StdChild" >0</p>
            </article>
        </td>
        <td>
            <article class="sub" onclick= recordTicket("B1")>
                <p>beanie 1</p>
                <p class = "ticketValue" id="beanie1" >0</p>
            </article>
        </td>
        <td>
            <article class="sub" onclick= recordTicket("B2")>
                <p>beanie 2</p>
                <p class = "ticketValue" id="beanie2" >0</p>
            </article>
        </td>
        <td>
            <article class="sub" onclick= recordTicket("B3")>
                <p>beanie 3</p>
                <p class = "ticketValue" id="beanie3">0</p>
            </article>
        </td>
    </tr>

    <caption>*  On a Bean-Bag, you may sit: <br>
        1 adult, or two adults, or an adult and a child,
        or three kids.</caption>
</table>
</html>
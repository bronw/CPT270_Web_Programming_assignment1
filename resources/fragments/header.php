<!DOCTYPE html>
<!--universal header for the Silverado page -->
<head>
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300italic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css"
          href="resources/styles/headerStyle.css" />
</head>

<header>
    <div id="LogoSlogan">
        <img id = "logoImg"
             src="resources/img/logoV3.jpg"
             alt="The Silverado GodFather"
             style = "height:6em;">


    </div>
    <nav>
        <ul>
            <li> <a href="index.php">Home</a></li>
            <li><a href="whatsNew.php">What's New</a></li>
            <li> <a href="schedules.php">Schedules</a> </li>
            <li> <a href=nowShowing.php>Now Showing</a> </li>
            <li><a href=booking.php> Booking </a></li>
        </ul>
    </nav>
    <!-- Dividing line -->
    <div class="line" id="headerLine"></div>

    <img id = "rosePic"
         src="resources/img/roseVersion5.jpg"

         alt="rose"
         style="width:48px;height:48px;">


</header>




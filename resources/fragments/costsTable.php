<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel = "stylesheet" href="resources/styles/tableStyle.css">
</head>
<table id ="costsTable">
    <tr>
        <th>Seating</th>
        <th colspan="2">Gold Class</th>
        <th colspan="3">Standard</th>
        <th>Bean-Bag*</th>
    </tr>

    <tr>
        <th>Ticket</th>
        <th> adult </th>
        <th> child</th>
        <th> full</th>
        <th> concession</th>
        <th> child</th>
        <th></th>

    </tr>

    <tr>
        <th>Monday</th>
        <td rowspan="2"> $25 </td>
        <td rowspan="2"> $20</td>
        <td rowspan="2"> $12</td>
        <td rowspan="2"> $10</td>
        <td rowspan="2"> $8</td>
        <td rowspan="2"> $20</td>

    </tr>
    <tr>
        <th>Tuesday</th>
    </tr>

    <tr>
        <th > Wednesday</th>
        <td rowspan="5"> $30 </td>
        <td rowspan="5"> $25</td>
        <td rowspan="5"> $18</td>
        <td rowspan="5"> $15</td>
        <td rowspan="5"> $12</td>
        <td rowspan="5"> $30</td>
    </tr>
    <tr>    <th>Thursday</th> </tr>
    <tr>    <th>Friday</th> </tr>
    <tr>    <th>Saturday</th> </tr>
    <tr>    <th>Sunday</th> </tr>

    <tr>
        <th>The 1:00pm session <br> (Monday-Friday)</th>
        <td > $25 </td>
        <td > $20</td>
        <td > $12</td>
        <td > $10</td>
        <td > $8</td>
        <td > $20</td>
    </tr>
    <caption>*  On a Bean-Bag, you may sit: <br>
        1 adult, or two adults, or an adult and a child,
        or three kids.</caption>
</table>
</html>
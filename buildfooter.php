<!DOCTYPE html>
<html lang="en">
<head> <!-- repeated throughout  -->
    <meta charset="UTF-8">
    <title>Silverado - home</title>

    <link rel="stylesheet" type="text/css"
          href="resources/styles/generalStyle.css" />

    <!-- fonts -->
    <link href='https://fonts.googleapis.com/css?family=Simonetta:400,900italic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css"
          href="http://fonts.googleapis.com/css?family=Tangerine">

       <style>

        #testSeperater{
            display: block;
            width:100%;
            height:500px;
            background-color: #E6AC00;
        }
    </style>




</head>
<body>
<!-- Defining the #page section-->
<section class="page">
    <!-- header fragment  -->
    <article class = "headingFragment">
        <?php require('resources/fragments/header.php') ?>
    </article>


<div id="testSeperater"></div>




    <!-- footer fragment  -->
    <article class = "footerFragment">
        <?php require('resources/fragments/footer.php') ?>
    </article>



</section>
</body>
</html>
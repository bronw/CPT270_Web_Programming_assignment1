<!DOCTYPE html>
<html lang="en">
<head> <!-- repeated throughout  -->
    <meta charset="UTF-8">
    <title>book Movie</title>

    <!-- styles -->

    <link rel="stylesheet" type="text/css"
          href="resources/styles/bookingStyle.css" />

    <link rel="stylesheet" type="text/css"
          href="resources/styles/generalStyle.css" />

    <!-- fonts -->
    <link href='https://fonts.googleapis.com/css?family=Simonetta:400,900italic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css"
          href="http://fonts.googleapis.com/css?family=Tangerine">


    <!-- javascripts -->
    <script type="text/javascript" src="resources/scripts/bookingScript.js"></script>

</head>





<body>
<!-- Defining the #page section-->
<section class="page">
    <!-- header fragment  -->
    <article class = "headingFragment">
        <?php require('resources/fragments/header.php') ?>
    </article>

<div id="bookingPage">





    <!-- movie picker fragment  -->
    <H2>Pick a movie and screening time</H2>
    <section id="chooseMovie">
        <?php require('resources/fragments/movieSelectionTable.php') ?>
    </section>


    <!-- display movie fragment  -->
    <section id="displayMovie">
        <?php require('resources/fragments/movieDisplay.php') ?>
    </section>


    <!-- ticket chooser fragment  -->
    <H2 id="ticketChooser">Choose tickets</H2 >
    <section id="chooseMovie">
        <?php require('resources/fragments/ticketCostsTable.php') ?>
    </section>



    <article id = "orderRecord">
        <fieldset>
            <button type="reset"
                    class="actionsBtns"
                    onclick = resetSelectRecordsFrom()
                    >Start Over</button>
        </fieldset>

        <!-- details text area  -->
        <fieldset>
            <legend>Order Summary</legend>
                <textarea
                    id =   displayOrder
                    name = displayOrder
                    readonly
                    rows = 10
                    wrap = hard
                    placeholder="your order will be shown here"
                ></textarea>
        </fieldset>

        <fieldset>
            <button type=submit
                    id="submitBtn"
                    class="actionsBtns"
                    onclick=submitSelectRecordsFrom()
                    disabled
                    >Buy Now!</button>
        </fieldset>

    </article>



</div>
    <!-- footer fragment  -->
    <article class = "footerFragment">
        <?php require('resources/fragments/footer.php') ?>
    </article>

    <!-- records of order  -->
<form id="selectionRecords"
      action="http://titan.csit.rmit.edu.au/~e54061/wp/testbooking.php"
      method="post">
    <fieldset>
        <legend>Movie Information</legend>
            <label for="day">day</label><input id="day" name ="day">
            <label for="time">time</label><input id="time" name="time">
            <label for="movie">name</label><input id="movie" name="movie">
            <label for="name">name</label><input id="name" name="name">
            <label for="movieTitle">movieTitle</label><input id="movieTitle">
    </fieldset>
    <fieldset>
        <legend>Derived Information</legend>
            <label for=movieString>movieString</label><input id =movieString>
            <label for=priceGroup>priceGroup</label><input id =priceGroup>
            <label for=ticketString>ticketString</label><input id =ticketString>

    </fieldset>
    <fieldset>
        <legend>Ticket Information</legend>
            <label for=SA>SA</label><input class = "ticketInfo"
                                           name =SA
                                           id="Std Adult">
            <label for=SP>SP</label><input class = "ticketInfo"
                                           name =SP
                                           id="Std Concession">
            <label for=SC>SC</label><input class = "ticketInfo"
                                           name =SC
                                           id="Std Child">

            <label for=FA>FA</label><input class = "ticketInfo"
                                           name =FA
                                           id="GoldClassAdult">
            <label for=FC>FC</label><input class = "ticketInfo"
                                           name =FC
                                           id="GoldClassChild">

            <label for=B1>B1</label><input class = "ticketInfo"
                                           name =B1
                                           id="Beanie 1">
            <label for=B2>B2</label><input class = "ticketInfo"
                                           name =B2
                                           id="Beanie 2">
            <label for=B3>B3</label><input class = "ticketInfo"
                                           name =B3
                                           id="Beanie 3">
    </fieldset>
</form>




</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head> <!-- repeated throughout  -->
    <meta charset="UTF-8">
    <title>contact silverado</title>

    <link rel="stylesheet" type="text/css"
          href="resources/styles/generalStyle.css" />

    <link rel="stylesheet" type="text/css"
          href="resources/styles/contactStyle.css" />

    <!-- fonts -->
    <link href='https://fonts.googleapis.com/css?family=Simonetta:400,900italic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css"
          href="http://fonts.googleapis.com/css?family=Tangerine">






</head>
<body>
<!-- Defining the #page section-->
<section class="page">
    <!-- header fragment  -->
    <article class = "headingFragment">
        <?php require('resources/fragments/header.php') ?>
    </article>





    <!-- form  -->
    <form id = contactForm
          action="http://titan.csit.rmit.edu.au/~e54061/wp/testcontact.php"
          method="post">
        <fieldset>
            <legend>Choose a subject</legend>

            <ol>
                <li>
                    <ul>
                        <li>
                            <input
                                id=generalEnquiry
                                name=subject
                                type = radio
                                required>
                                <!--checked>-->
                            <label for=generalEnquiry>General Enquiry</label>
                        </li>
                        <li>
                            <input
                                id=groupBookings
                                name=subject
                                type = radio
                                required>
                            <label for=groupBookings>Group and Corporate Bookings</label>
                        </li>
                        <li>
                            <input
                                id=suggestions
                                name=subject
                                type = radio
                                required>
                            <label for=suggestions>Suggestions & Complaints</label>
                        </li>
                    </ul>
                </li>
            </ol>
        </fieldset>
        <fieldset>
            <legend>Enter your contact details and message</legend>
            <ol>

                <li>
                    <label for = email>Email</label>
                    <input
                        id=email
                        name = email
                        type = email
                        placeholder = "youHandle@SomeShonkyWebsite.someWhere"
                        required
                        autofocus>
                </li>
                <li>

                    <label for=message>Message</label>
                    <textarea
                        id =message
                        name = message
                        rows = 5
                        wrap = hard
                        placeholder="enter your comments here"
                        required></textarea>

                </li>

            </ol>
        </fieldset>

        <fieldset>
            <legend>Lodge it</legend>
            <button type=submit>Send!</button>
        </fieldset>
    </form>













    <!-- footer fragment  -->
    <article class = "footerFragment">
        <?php require('resources/fragments/footer.php') ?>
    </article>
</section>
</body>
</html>
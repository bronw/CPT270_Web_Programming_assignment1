Web Programming
Assignment 1
Marks Allocation
20 marks or 20% of your final grade

1. Website structure (4 marks):
	Homepage content reflects business, is thought out, well written 1 mark
	HTML5 code, uniform page structure with 4 distinct areas completed 1 mark
	Prices and Schedule information shown 1 mark
	Above Information displayed in a clear, attractive and thoughtful way 1 mark

2. Navigation, Style and Layout (4 marks):
	Navigation bar and links are styled to a high standard (eg buttonlike)
	1 mark
	Colour scheme makes sense, good for those with vision difficulties 1 mark
	Two or more web fonts are used and are attractive and readable 1 mark
	All styling (ie 95% or more) is external CSS, no deprecated HTML4 code 1 mark

3. Contact Page and Form (4 marks):
	All fields present with correct values and are correct type 1 mark
	Labels are clear, fields line up neatly, aren't too big or small 1 mark
	If fields are blank, submission is blocked, user is alerted 1 mark
	Submits correctly named data and values using the post method 1 mark

4. Movie Bookings Page and Form (4 marks):
	Basic movie information is displayed correctly 1 mark
	Movie panels reveal more details when clicked, hovered over etc 1 mark
	Choice of form fields are appropriate, totals and subtotals present 1 mark
	Form submits all data to correct url, data is correctly named 1 marks

5. Javascript / jQuery Component (4 marks):
	Client can only select movies at a time when they are actually screening 1 mark
	The prices are calculated correctly for discounted and full price screenings 1 mark
	The prices are calculated in real time (ie as the customer makes selections) 1 mark
	The prices are displayed to two decimal places 1 mark

Total: 20 marks (or 20% of your final grade)
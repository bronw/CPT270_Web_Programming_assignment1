<!DOCTYPE html>
<html lang="en">
<head>
    <title>movies</title>

    <meta charset="UTF-8">
    <link rel="stylesheet" href="resources/styles/nowShowing.css">

    <link rel="stylesheet" type="text/css"
              href="resources/styles/generalStyle.css" />

<!--    <!-- fonts -->
    <link href='https://fonts.googleapis.com/css?family=Simonetta:400,900italic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css"
          href="http://fonts.googleapis.com/css?family=Tangerine">



</head>
<body>

<section class = "page">

    <!-- header -->
    <article class = "headingFragment">
        <?php require('resources/fragments/header.php') ?>
    </article>

    <h1 > Now On at the Silverado</h1>
    <p> We mixed the great with the epic and, for you! Right now! We have
        these breath-taking movies.
    </p>

    <!-- details -->
    <article id="NowShowingPage">
        <div class="section__content clearfix ">
            <h2 >Love Actually</h2>

            <article class = "summary romanticWords">
                <p> Just a guy, standing in front of a girl,
                    asking her to 'pull my finger'. </p>
            </article>

            <div class="card romantic effect__hover">
                <div class="card__front">
                    <img src="resources/img/loveActually2.jpg"
                         alt="Romantics"
                         height="95%">
                </div>
                <div class="card__back">
                    <p>After which the Silverado's extra-ply <i>‘tissues
                        actually’ </i> tissue-box was named, this movie
                        is a vague conglomerate of unrelated stories
                        played-out across the city of London. Each
                        descending inexorably towards their gruesome,
                        but heart-warming end.<br><br>

                        Recommended to all ages, but if you are with
                        your granny… get two boxes. </p>

                    <div class ="btn">
                        <a href="booking.php">ROMANCE ME!</a>
                    </div>

                </div>
            </div>
        </div><!-- /card -->


    <div class="section__content clearfix">
        <h2 >Goldfinger</h2>

        <article class = "summary actionWords">
            <p> Ahh Mr Bond, we meet again.
            Have you seen my new <i> Laser beam </i>?</p>
        </article>

        <div class="card act effect__hover">
            <div class="card__front">
                <img src="resources/img/goldfinger_poster.jpg"
                     alt="actionable"
                     height="95%">
            </div>
            <div class="card__back">
                <p>Dr No died like no doctor should, but Bond returned.
                    And painted the town red, and the women gold - and Ms
                    Moneypenny any which way she liked. <br><br>

                    In its day, it was brutal and sexy and graphic.
                    It was surpassed for all those qualities by The Ren
                    and Stimpy Cartoon show in the late 1900’s.
                    Take the Kids? Knock yourself out.
                </p>

                <div class ="btn">
                    <a href="booking.php">ACTION ME!</a>
                </div>

            </div>
        </div>
    </div><!-- /card -->


    <div class="section__content clearfix">
        <h2 >Brave</h2>

        <article class = "summary kidWords">
            <p> Pixar's Fantastic Feminist Documentary</p>
        </article>

        <div class="card kid effect__hover">
            <div class="card__front">
                <img src="resources/img/brave-620x266.png"
                     alt="braveArrowShooter"
                     height="95%">
            </div>
            <div class="card__back">
                <p>What can I say about this movie? Not much
                    because I went and mowed the lawn while it
                    was on… kids liked it but.
                </p>

                <div class ="btn">
                    <a href="booking.php">SPANK ME!</a>
                </div>

            </div>
        </div>
    </div><!-- /card -->


    <div class="section__content clearfix">
        <h2 >Kamasutra 3-D</h2>

        <article class = "summary artWords">
            <p> The epics of all epics</p>
        </article>

        <div class="card arts effect__hover">
            <div class="card__front">
                <img src="resources/img/Kamasutra-3D-poster.jpg"
                     alt="kamasutra"
                     height="95%">
            </div>
            <div class="card__back">
                <p>This, people, is why we got 3D projectors
                    and the big sound! I've said it before: we
                    might live in a small
                    town, but we aren’t small-town. <br> <br>

                    Definitely don't bring the kids (we will do a
                    Bollywood night for them later).

                </p>

                <div class ="btn">
                    <a href="booking.php">LOOK AT MOI!</a>
                </div>


            </div>
        </div>
    </div><!-- /card -->

    </article>


    <!-- footer  -->
    <article class = "footerFragment">
        <?php require('resources/fragments/footer.php') ?>
    </article>
</section>

</body>
</html>
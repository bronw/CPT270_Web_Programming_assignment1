<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head> <!-- repeated throughout  -->
    <title>Silverado - home</title>

    <link rel="stylesheet" type="text/css"
          href="resources/styles/generalStyle.css" />

    <!-- fonts -->
    <link href='https://fonts.googleapis.com/css?family=Simonetta:400,900italic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css"
          href="http://fonts.googleapis.com/css?family=Tangerine">
</head>
<body>
<!-- Defining the #page section-->
<section  class="page">
    <!-- header -->
    <article class = "headingFragment">
    <?php require('resources/fragments/header.php') ?>
    </article>


    <!-- details -->
    <article class = "mainContent">

        <h1>So... Silverardo opens again </h1>

        <figure id = "page1img"><img
            src="resources/img/MinChaire2.png"
            width = 300px
            alt="Michael in dad's chair." >
        </figure>

        <p>You know us ...we know you HERE HERE.</p>
        <p>We are the wise guys on the corner of Mains and Fourth Streets. We have new partners, the
            Corleone Family. <strong> Have your heard of them? </strong></p>

        <p>The Corleone's is getting out of the olive oil business, settling out here, in the
            west, western Sydney - go figure. </p>
        <p>You see, Don Michael Corleone says we gotta make the place nice.
            So we've been busy, wrecking the joint. Now it is nice, real nice. </p>

        <p> We have it all now. Here, in our town. Where you can park right out front.
                You know... being in the same town,
                it is important. It is more important then government,
                more important then sharing the same pub.
                It is almost like <span = class = "theFamily"> Family </span>.</p>





        <br>
        <a href ="whatsNew.php" class ="bigLink">Look what they did to my Sonny!</a>

    </article>


    <article class = "footerFragment">
        <?php require('resources/fragments/footer.php') ?>
    </article>


</section>
</body>
</html>

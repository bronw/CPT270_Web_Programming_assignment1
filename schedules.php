<!DOCTYPE html>
<html>
<head>
    <title>dollars 'n' times</title>


    <link rel="stylesheet" type="text/css"
          href="resources/styles/scheduleStyle.css">

    <link rel="stylesheet" type="text/css"
          href="resources/styles/generalStyle.css">


    <!-- fonts -->
    <link href='https://fonts.googleapis.com/css?family=Simonetta:400,900italic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css"
          href="http://fonts.googleapis.com/css?family=Tangerine">

    <Style>
    #schedulesPage{
        display: block;
        width:1000px;
        height:2500px;
        margin:0 auto;
        position:relative;
        background-color: black;
    }

    </Style>
</head>
<body>
<!-- Defining the #page section-->
<section class="page">


    <!-- get header -->
    <article class = "headingFragment">
        <?php require('resources/fragments/header.php') ?>
    </article>


    <div id = "CostsSelector">
        <span ID = "spanCosts">How much will it cost?</span>


    <div ID="costsTable">
        <?php require('resources/fragments/costsTable.php') ?>
    </div>
    </div>


    <div id = "timeSelector">
       <span id = "spanTime">When should I go?</span>

        <div id="timeTable">
            <?php require('resources/fragments/screeningTimesTable.php') ?>
        </div>
    </div>


    <!-- footer  -->
    <article id = "footerFragment">
        <?php require('resources/fragments/footer.php') ?>
    </article>

</section>
</body>
</html>